﻿######################################################################
######           Copyright (c) 2016 BigSQL                      ######
######################################################################

$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path


$a = New-Object -ComObject Scripting.FileSystemObject 
$f = $a.GetFolder($ScriptDir)

$PGC_HOME = $f.ShortPath
$PGC_LOGS = Join-Path $f.ShortPath logs\pgcli_log.out

$env:PGC_HOME=$PGC_HOME
$env:PGC_LOGS=$PGC_LOGS


function logtoFile([string]$msg){
    
    $wmi = Get-WmiObject -Class Win32_OperatingSystem
    $currenttime=$wmi.LocalDateTime.ToString()
    $datetimeFormat = -join(
        $currenttime.substring(0,4),"-",
        $currenttime.substring(4,2),"-",
        $currenttime.substring(6,2)," ",
        $currenttime.substring(8,2),":",
        $currenttime.substring(10,2),":",
        $currenttime.substring(12,2))
    $logmsg = -join($datetimeFormat, " [INFO] : ", $msg);
    $logmsg | Out-File $PGC_LOGS -Append

}

# Main Line
$hub_new = Join-Path $PGC_HOME hub_new
$FileExists = Test-Path $hub_new
If ($FileExists -eq $True) {
    $hub_upgrade = Join-Path $PGC_HOME hub_upgrade
    $hub_old = Join-Path $PGC_HOME hub_old
    Rename-Item $hub_new $hub_upgrade
    logtoFile("completing hub upgrade")
    Rename-Item hub hub_old
    copy-item $hub_upgrade\* $PGC_HOME -force -Recurse
    Remove-Item $hub_old -force -Recurse
    Remove-Item $hub_upgrade -force -Recurse
    logtoFile("hub upgrade completed.")
}

$hub_script = Join-Path $PGC_HOME hub\scripts
$hub_scripts_lib = Join-Path $PGC_HOME hub\scripts\lib

$env:PYTHONPATH="$hub_script;$hub_scripts_lib"

$python2=Join-Path $PGC_HOME python2
$isPython = Test-Path $python2

If ($isPython -eq $True) {
   $env:PATH="$python2;$env:PATH"
   $env:PYTHONHOME="$python2"
}

python -u $hub_script\pgc.py $args
