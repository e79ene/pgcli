@echo off
setlocal

@REM ###################################################
@REM #########   Copyright (c) 2016 BigSQL    ##########
@REM ###################################################

set SCRIPT_DIR=%~sdp0

%SYSTEMROOT%\System32\WindowsPowerShell\v1.0\powershell.exe -ExecutionPolicy ByPass -NoProfile "%SCRIPT_DIR%pgc.ps1" %1 %2 %3 %4 %5 %6 %7 %8 %9
